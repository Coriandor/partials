<?php
/**
 * Plugin Name: Partials
 * Description: Allows snippets of content that to be inserted into pages and posts through short tags.
 * Version: 1.0.1
 * Author: Joel Kuhn
 * License: GPL2
 */

namespace Partials;


add_action('media_buttons', '\partials\add_media_button', 100);
function add_media_button() {
	$is_post_edit_page = in_array(RG_CURRENT_PAGE, array('post.php', 'page.php', 'page-new.php', 'post-new.php'));
    if(!$is_post_edit_page)
        return;

    include 'add-partial.php';
}

add_shortcode('partial', '\partials\insert_partial');
function insert_partial($atts) {
	$base_partials_dir = get_template_directory() . DIRECTORY_SEPARATOR . 'partials';

	$partial_file = $base_partials_dir . DIRECTORY_SEPARATOR . $atts[0] . '.php';
	if (file_exists($partial_file)) {
		ob_start();
		require $partial_file;
		return ob_get_clean();
	}

	$partial_dir = $base_partials_dir . DIRECTORY_SEPARATOR . $atts[0];
	$partial_url = get_template_directory_uri() . '/partials/' . $atts[0];

	$partial_html = $partial_dir . DIRECTORY_SEPARATOR . $atts[0]. '.html';
	$partial_php = $partial_dir . DIRECTORY_SEPARATOR . $atts[0]. '.php';
	$partial_js = $partial_dir . DIRECTORY_SEPARATOR . $atts[0]. '.js';
	$partial_css = $partial_dir . DIRECTORY_SEPARATOR . $atts[0]. '.css';

	$include_dir = $partial_dir . DIRECTORY_SEPARATOR . 'includes';
    if (file_exists($include_dir) && is_dir($include_dir)) {
        if ($handle = opendir($include_dir)) {

            while (false !== ($entry = readdir($handle))) {
            	$reversed = strrev($entry);
            	if (stripos($reversed, 'ssc.') === 0) {
	        		$file_url = $partial_url . '/includes/' . $entry;
            		wp_enqueue_style($entry, $file_url);
            	}
            	else if (stripos($reversed, 'sj.') === 0) {
	        		$file_url = $partial_url . '/includes/' .$entry;
            		wp_enqueue_script($entry, $file_url);
            	}
            }

            closedir($handle);
        }
    }

	if (file_exists($partial_js)) {
		$template_url = get_template_directory_uri();
		wp_enqueue_script($atts[0] . '-partial-script', $template_url . '/partials/' . $atts[0] . '/' .$atts[0] . '.js');
	}

	if (file_exists($partial_css)) {
		$template_url = get_template_directory_uri();
		wp_enqueue_style($atts[0] . '-partial-style', $template_url . '/partials/' . $atts[0] . '/' . $atts[0] . '.css');
	}

	if (file_exists($partial_html)) {
		return file_get_contents($partial_html);
	}
	
	else if (file_exists($partial_php)) {
		global $partial_atts;
		$partial_atts = $atts;
		ob_start();
		require $partial_php;
		return ob_get_clean();
	}
}
