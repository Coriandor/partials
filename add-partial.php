<a href="#TB_inline?width=400&height=400&inlineId=select_partial" class="thickbox button">Add Partial</a>
<?php global $partial_add_buttton_code; ?>
<?php if (!isset($partial_add_buttton_code)): ?>
<script>
    function InsertPartial(){
        var partial_name = jQuery("#add_partial").val();
        if (partial_name == "") {
            alert("Please select a partial");
            return;
        }

        window.send_to_editor("[partial " + partial_name + "]");
    }
</script>

<div id="select_partial" style="display:none;">
    <div>
        <div>
            <div style="padding:15px 15px 0 15px;">
                <h3>Select a Partial</h3>
                <span>
                    Select a partial you would like to include in the page.
                </span>
            </div>
            <div style="padding:15px 15px 0 15px;">
                <select id="add_partial">
                    <option value="">Select a Partial.</option>
                    <?php
                        $partials_dir = get_template_directory() . DIRECTORY_SEPARATOR . 'partials';
                        if (file_exists($partials_dir) && is_dir($partials_dir)) {
                            if ($handle = opendir($partials_dir)) {

                                while (false !== ($entry = readdir($handle))) {
                                    if ($entry == '.' || $entry == '..') continue;

                                    ?>
                                        <option><?php echo $entry; ?></option>
                                    <?php
                                }

                                closedir($handle);
                            }
                        }
                    ?>
                </select>
            </div>
            <div style="padding:15px;">
                <input type="button" class="button-primary" value="Insert Partial" onclick="InsertPartial();"/>&nbsp;&nbsp;&nbsp;
            <a class="button" style="color:#bbb;" href="#" onclick="tb_remove(); return false;">Cancel</a>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<?php $partial_add_buttton_code = true; ?>